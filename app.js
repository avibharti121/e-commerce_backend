const express = require("express");
const app = express();
const dbcon = require("./config/config")
const cookieParser = require('cookie-parser');
const path = require("path")
const port = 8800;
app.use(cookieParser())
// parse URL encoded bodies (as sent by html forms)
app.use(express.urlencoded({ extended: false }));
app.use(express.json())

var cors = require('cors');
app.use(cors());
app.use('./upload', express.static('.upload'));
// const public_dir = path.join(__dirname, "./public");
// app.use(express.static(public_dir));
app.use(express.static('upload'));   
app.use('/upload', express.static('upload')); 


// use hbs templete
// app.set('view engine', 'hbs');

// Define routes
// app.use('/',require('./routes/page'));

app.use('/',require('./src/app/routes/routes'));
app.use('/',require('./src/app/controller/product'));

app.listen(port, () => {
    console.log("Server start at " + port)
})