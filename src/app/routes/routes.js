const express = require('express');
const router = express.Router();
const auth_c= require('../controller/signup_login')
const product_c= require('../controller/product')


router.post('/register',auth_c.register);


router.post('/login',auth_c.login);

router.post('/logout',auth_c.logout);

// router.get('/',(req,res)=>{ 
//     return res.status(200).json({
//         status: false,
//         message: "Api home get working",
//         status:200
       
//       });
    
   
// })

router.get('/set',product_c)
module.exports=router;