const db = require("../../../config/config");
var jwt = require('jsonwebtoken');
const express = require("express");
const multer = require("multer");
const router = express.Router();


const storage = multer.diskStorage({
    destination: (req, res, cb) => {
        cb(null, './upload')//folder path
    },
    filename: (req, file, cb) => {
        cb(null, Date.now()+file.originalname)
    }
})

var upload = multer({ storage: storage })



//single file
router.post('/upload', upload.single('product_img'), (req, res) => {
    var fileinfo = req.file;

    const { user_id, product_title, product_desc, product_variant, product_price } = req.body;

    const product_img = req.file.filename;

    // console.log(user_id + " "+product_title + " " + product_desc +  " " + product_img + " "  + product_variant + " "  + product_price );
    if (!product_title || !product_desc || !product_variant || !product_price || !product_img || !user_id) {

        return res.status(400).json({
            status: false,
            message: "Please enter all field",
            status: 400,

        });
    } else {
        db.query(
            "select id from users where id=?", [user_id], (error, result) => {
                if (error) {
                    console.log(error);
                    return res.status(400).json({
                        status: false,
                        message: "Invalid User Id",
                        status: 400,

                    });
                    
                }
                else if (result.length) {


                    if (result.length > 0) {

                        const product_data = {
                            user_id: user_id,
                            product_title: product_title,
                            product_desc: product_desc,
                            product_img: product_img,
                            product_variant: product_variant,
                            product_price: product_price
                        }

                        db.query(
                            "INSERT INTO products SET ?", product_data,

                            (error, result) => {

                                if (error) {
                                    console.log(error);
                                } else {

                                    return res.status(200).json({
                                        status: false,
                                        message: "Product Added Successfully",
                                        status: 200,

                                    });
                                }
                            }
                        );
                    }
                } else {
                    return res.status(400).json({
                        status: false,
                        message: "Enter Valid User Id",
                        status: 400

            });
        }
    });
}

});


//multiple file uploads..................

router.post('/uploads', upload.array('img',4), (req, res) => {
    var fileinfo = req.files;
    var product_id = req.body.product_id;
    var product_array=[];
  

    fileinfo.forEach(function (arrayItem,index) {
        var x = arrayItem.filename;
        
        product_array.push(
            {
            product_id:product_id,
            product_img_name:x
            
        })  
    });
    // return res.json(product_array);
product_array.forEach((iteam,index)=>{

    db.query(
        "INSERT INTO product_img SET ?", iteam,

        (error, result) => {

            if (error) {
                console.log(error);
            } 
            else {

                return res.status(200).json({
                    status: false,
                    message: "Product Added Successfully",
                    status: 200,

                });
            }
        }
    );

  })

});

///////////////////////////////////




// multi img view................
router.get('/views',(req, res) => {
    db.query("SELECT product_id ,product_img_name from product_img", (err,results) =>{
        if(err){
            return console.error(err.message);
        }
        res.status(200).json(results)

        // res.status(200).json({
        //     success: true,
        //     message: "Successfully View",
        //     data:results
        // });
    });
})



///////////////////////

router.get('/view',(req, res) => {
    db.query("SELECT user_id,product_title,product_desc,product_img,product_variant,product_price from products", (err,results) =>{
        if(err){
            return console.error(err.message);
        }
        res.status(200).json({
            success: true,
            message: "Successfully View",
            data:results
        });
    });
})




module.exports = router;

