-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 23, 2021 at 01:00 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `api2`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_title` varchar(50) DEFAULT NULL,
  `product_desc` text DEFAULT NULL,
  `product_img` text NOT NULL,
  `product_variant` enum('Large','Medium','Small','') DEFAULT NULL,
  `product_price` int(11) DEFAULT NULL,
  `is_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` datetime DEFAULT NULL,
  `is_blocked` tinyint(1) DEFAULT NULL,
  `is_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `product_title`, `product_desc`, `product_img`, `product_variant`, `product_price`, `is_created`, `is_deleted`, `is_blocked`, `is_update`) VALUES
(10, 1, 'Laptop_1', 'intel i5 RAM 16Gb', '1619173497418pic7.jpeg', 'Large', 2000000, '2021-04-23 10:24:57', NULL, NULL, '2021-04-23 10:24:57'),
(11, 1, 'Laptop_msi', 'intel i7 RAM 16Gb', '1619173531277pic6.jpeg', 'Large', 32000, '2021-04-23 10:25:31', NULL, NULL, '2021-04-23 10:25:31'),
(12, 1, 'Laptop_Hp', 'intel i7 RAM 16Gb', '1619173551365pic5.jpeg', 'Large', 42000, '2021-04-23 10:25:51', NULL, NULL, '2021-04-23 10:25:51'),
(13, 1, 'Laptop_acer', 'AMD , RAM 16Gb', '1619173600231pic4.jpeg', 'Large', 62000, '2021-04-23 10:26:40', NULL, NULL, '2021-04-23 10:26:40'),
(14, 1, 'Lg smart Tv', 'Full Hd  5 inch', '1619173652321pic3.jpeg', 'Large', 42000, '2021-04-23 10:27:32', NULL, NULL, '2021-04-23 10:27:32'),
(15, 1, 'TCL smart Tv', 'Full Hd  50 inch', '1619173671818pic2.jpeg', 'Large', 42000, '2021-04-23 10:27:51', NULL, NULL, '2021-04-23 10:27:51'),
(16, 1, 'TCL smart Tv', 'Full Hd  50 inch', '1619174336323pic2.jpeg', 'Large', 42000, '2021-04-23 10:38:56', NULL, NULL, '2021-04-23 10:38:56'),
(17, 1, 'TCL smart Tv', 'Full Hd  50 inch', '1619174337188pic2.jpeg', 'Large', 42000, '2021-04-23 10:38:57', NULL, NULL, '2021-04-23 10:38:57');

-- --------------------------------------------------------

--
-- Table structure for table `product_img`
--

CREATE TABLE `product_img` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_img_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_img`
--

INSERT INTO `product_img` (`id`, `product_id`, `product_img_name`) VALUES
(18, 10, '1619175496018pic3.jpeg'),
(19, 10, '1619175496019pic3.jpeg'),
(20, 10, '1619175496020pic2.jpeg'),
(21, 12, '1619175588004asus-original-imafs5qf3f5cdp5t.jpeg'),
(22, 12, '1619175588006pic2.jpeg'),
(23, 10, '1619175619176asus-original-imafs5qf3f5cdp5t.jpeg'),
(24, 10, '1619175619179pic2.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `token` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `token`) VALUES
(1, 'Avi', 'avi@gmail.com', '$2b$10$.AjSmgg.XqzywN61p4jMLutomT4VLCIrme5z1axOUM52tZ1e782Ge', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImF2aUBnbWFpbC5jb20iLCJpYXQiOjE2MTkwMDMyNDYsImV4cCI6MTYxOTAwMzM2Nn0.SiQpi-SZIJv9-eVXacbUfkPtG-8VS9xbcoi_1hMnILo'),
(2, 'Abhishek', 'THEROCKSTARMONU@GMAIL.COM', '$2b$10$NHCo7q7p8Y7e58Rz7N0u3ek3JT8IA/mb7lFPyk6nIn2RPqyiLSOzS', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IlRIRVJPQ0tTVEFSTU9OVUBHTUFJTC5DT00iLCJpYXQiOjE2MTkwMDQwMDgsImV4cCI6MTYxOTAwNDEyOH0.cpt3CslJzY348WJanaDeuQamjvSXUjJqH78JhXaOS_Q'),
(3, 'Anand', 'anand@gmail.com', '$2b$10$JeRgEyVFvJMOJ1d5O0/duutLtctZ26CXv6OxYip3MBNbRyhcxnGgm', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImFuYW5kQGdtYWlsLmNvbSIsImlhdCI6MTYxOTAwNDI0OCwiZXhwIjoxNjE5MDA0MzY4fQ.kPlbODr1pqThyu9vfdZ8uEi7X3MbyiSm9hw0JB5hIhY'),
(4, 'Avi', 'fdgfgsgs', '$2b$10$6ROY54MdIUg5CdDNJEFnx.6eSq9/72Avc1kSMce6d8BeUvTXAwD1q', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImZkZ2Znc2dzIiwiaWF0IjoxNjE5MDc3MjExLCJleHAiOjE2MTkwNzczMzF9.EA3G7NJDW7ghor9KHu2DdyyvG6VlBsPSnFIqQNQkWBk'),
(5, 'Avi', 'avi123@gmail.com', '$2b$10$8Dh4QBbCW1Rf15EjddW7S.3d0w8IJupLKbTvTQz5HhWsOX65f7R.m', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImF2aTEyM0BnbWFpbC5jb20iLCJpYXQiOjE2MTkwNzcyNTgsImV4cCI6MTYxOTA3NzM3OH0._YlL3poaHahJ-583-LipqpZO6Rzr_Hu0BHKmrZnVl9M'),
(6, 'yamini', 'yamini@gmail.com', '$2b$10$sZ4MowfcN.idUVNIupcNiu9B5dH2PsaIYcBHOB16G96.oxMg1GauG', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6InlhbWluaUBnbWFpbC5jb20iLCJpYXQiOjE2MTkwNzg4NjUsImV4cCI6MTYxOTA3ODk4NX0.T6M0Dh-d9WccXf9sSxu4XCBnlMP_Qs2A_i0Gd4l1DJQ'),
(7, 'yamini', 'yamini1@gmail.com', '$2b$10$YO3l4b2nkGSwNj0Hkq/4t.tjKEGnERnZIJUAaeQTS45.V2GKrwty.', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6InlhbWluaTFAZ21haWwuY29tIiwiaWF0IjoxNjE5MDc4OTA2LCJleHAiOjE2MTkwNzkwMjZ9.eZ1kA4yQp27eLOS_WYwFoE-dfPk10c6pX-YGS6aqYGM');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_ibfk_1` (`user_id`);

--
-- Indexes for table `product_img`
--
ALTER TABLE `product_img`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `product_img`
--
ALTER TABLE `product_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_img`
--
ALTER TABLE `product_img`
  ADD CONSTRAINT `product_img_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
